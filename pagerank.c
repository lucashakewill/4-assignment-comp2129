#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <pthread.h>
#include <immintrin.h>

#include "pagerank.h"


bool convergence(double *old, double *new, int npages);

void pagerank(node* list, int npages, int nedges, int nthreads, double dampener) {

	double *oldrank = malloc(sizeof(double)*npages);
	double *newrank = malloc(sizeof(double)*npages);
	node *tmp;
    node *inlinks;

	// initialise each initial score to 1/N
	for (int i = 0; i<npages; i++)
	{
		oldrank[i] = 1/(double)npages;
	}

	int iteration = 0;
	
    //this loop breaks if convergence is true. 
    while (true)
	{
        tmp = list; //move through the list of pages 
        
		//iterate through each page
		for (int i = 0; i < npages; i++)
		{
		//iterate through the inlinks to the current page
			double sumins = 0;
            inlinks = tmp->page->inlinks;
			while(inlinks)
			{
                sumins += (oldrank[inlinks->page->index]/(double)inlinks->page->noutlinks); //needs to be the set of all pages that LINK TO u

			inlinks = inlinks->next;
		  }

			newrank[tmp->page->index] = (1-dampener)/(double)npages + dampener*sumins;

			tmp = tmp->next;
		}

		if(convergence(oldrank, newrank, npages))
			break;

		free(oldrank);
		oldrank = newrank;
		newrank = (double*)malloc(sizeof(double)*npages);
        //memcpy instead?
		iteration++;
    }
 tmp = list;
    
while (tmp)
{
printf("%s %.4lf\n", tmp->page->name, newrank[tmp->page->index]);
    tmp = tmp->next;
}
free(oldrank);
free(newrank);


	/*
		TODO
		- implement this function
		- implement any other necessary functions
		- implement any other useful data structures
	*/
}
/* check the convergence condition given the array of previous scores and current scores */
bool convergence(double *old, double *new, int npages)
{
	double sum = 0;

	for (int i = 0; i<npages; i++)
	{
		sum += pow((new[i] - old[i]), 2);
	}

	sum = sqrt(sum);
	if (sum <= EPSILON)
	{
		return true;
	}
	return false;
}

/*
######################################
### DO NOT MODIFY BELOW THIS POINT ###
######################################
*/

int main(int argc, char** argv) {

	/*
	######################################################
	### DO NOT MODIFY THE MAIN FUNCTION OR HEADER FILE ###
	######################################################
	*/

	config conf;

	init(&conf, argc, argv);

	node* list = conf.list;
	int npages = conf.npages;
	int nedges = conf.nedges;
	int nthreads = conf.nthreads;
	double dampener = conf.dampener;

	pagerank(list, npages, nedges, nthreads, dampener);

	release(list);

	return 0;
}
